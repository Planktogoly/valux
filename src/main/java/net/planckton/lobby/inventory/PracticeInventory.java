package net.planckton.lobby.inventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.Getter;
import net.planckton.lobby.ValuxLobby;
import net.planckton.lobby.utils.FancyItem;
import net.valux.ServerList;
import net.valux.redis.RedisMessenger;
import net.valux.values.messages.RedisPlayerSummonMessage;

public class PracticeInventory implements Listener,Runnable {
	
	@Getter private Inventory inventory;
	
	private FancyItem eu = new FancyItem(Material.POTION, 1, (byte) 8225).hideAllFlags()
			.name(ChatColor.AQUA.toString() + ChatColor.BOLD + "Practice-EU")
			.lore(ChatColor.GRAY + "Status: " + getStatus("pr_hub-eu"))
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-eu") 
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 1500)
			.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
	
	private FancyItem us = new FancyItem(Material.POTION, 1, (byte) 8233).hideAllFlags()
			.name(ChatColor.AQUA.toString() + ChatColor.BOLD + "Practice-US")
			.lore(ChatColor.GRAY + "Status: " + getStatus("pr_hub-us"))
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-us") 
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 1500)
			.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
	
	public PracticeInventory() {
		inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.AQUA + "Practice Selector");

		for (int i = 0; i < 27; i++) {
			inventory.setItem(i, new FancyItem(Material.STAINED_GLASS_PANE, 1, (byte) 15).name(" ").stack());
		}
		
		inventory.setItem(12, eu.stack());
		inventory.setItem(14, us.stack());
		
		Bukkit.getScheduler().runTaskTimer(ValuxLobby.getInstance(), this, 20, 20);
		Bukkit.getPluginManager().registerEvents(this, ValuxLobby.getInstance());
	}

	@Override
	public void run() {
		eu.clearLores();
		us.clearLores();
		
		eu.lore(ChatColor.GRAY + "Status: " + getStatus("pr_hub-eu"))
		.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-eu") 
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 1500)
		.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
		
		us.lore(ChatColor.GRAY + "Status: " + getStatus("pr_hub-us"))
		.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-us") 
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 1500)
		.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
		
		inventory.setItem(12, eu.stack());
		inventory.setItem(14, us.stack());
		
		for (HumanEntity entity : inventory.getViewers()) {
			Player player = (Player) entity;
			
			player.updateInventory();
		}		
	}
	
	private String getStatus(String server) {
		if (ServerList.contains(server)) return ChatColor.GREEN + "Online";
		
		return ChatColor.RED + "Offline";
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getInventory().getTitle() != null && !event.getInventory().getTitle().equalsIgnoreCase(inventory.getTitle())) return;		
		if (event.getCurrentItem() == null) return;
		
		ItemStack stack = event.getCurrentItem();
		ItemMeta meta = stack.getItemMeta();
		
		if (meta == null) return;	
		
		if (meta.getDisplayName().equalsIgnoreCase(eu.getName())) {
			RedisMessenger.submit(new RedisPlayerSummonMessage(event.getWhoClicked().getName(), "pr_hub-eu"));
			return;
		} else if (meta.getDisplayName().equalsIgnoreCase(us.getName())) {
			RedisMessenger.submit(new RedisPlayerSummonMessage(event.getWhoClicked().getName(), "pr_hub-us"));
			return;
		}
	}

}
