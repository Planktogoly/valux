package net.planckton.lobby.inventory;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.planckton.lobby.ValuxLobby;
import net.planckton.lobby.utils.FancyItem;
import net.valux.ServerList;
import net.valux.redis.RedisMessenger;
import net.valux.values.messages.RedisPlayerSummonMessage;

public class CompassInventory implements Runnable,Listener {
	
	@Getter private Inventory inventory;
		
	private FancyItem practice = new FancyItem(Material.POTION, 1, (byte) 16421).name(ChatColor.AQUA.toString() + ChatColor.BOLD + "Practice").hideAllFlags()
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + (ValuxLobby.getInstance().getServerCounts().get("practice-eu") + ValuxLobby.getInstance().getServerCounts().get("practice-us"))
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 3000);
	
	private FancyItem kitpvp = new FancyItem(Material.FIREWORK).name(ChatColor.YELLOW.toString() + ChatColor.BOLD + "KitPvP")
			.lore(ChatColor.GRAY + "Status: " + getStatus("kitpvp"))
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitpvp-eu") 
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500)
			.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
	
	private FancyItem kitmap = new FancyItem(Material.POTION, 1, (byte) 8226).name(ChatColor.GREEN.toString() + ChatColor.BOLD + "KitMap").hideAllFlags()
			.lore(ChatColor.GRAY + "Status: " + getStatus("kitmap"))
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitmap-eu") 
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500)
			.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
	
	private FancyItem hcf = new FancyItem(Material.STRING).name(ChatColor.RED.toString() + ChatColor.BOLD + "Hardcore Factions")
			.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("hcf") 
					+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500);
	
	public CompassInventory() {
		inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.AQUA + "Server Selector");

		ItemStack stack = practice.stack();
		
		Potion potion = new Potion(PotionType.INSTANT_HEAL);
		potion.setSplash(true);
		
		potion.apply(stack);
		
		for (int i = 0; i < 27; i++) {
			inventory.setItem(i, new FancyItem(Material.STAINED_GLASS_PANE, 1, (byte) 15).name(" ").stack());
		}
		
		inventory.setItem(10, stack);
		inventory.setItem(12, kitpvp.stack());
		inventory.setItem(14, hcf.stack());
		inventory.setItem(16, kitmap.stack());
		
		Bukkit.getScheduler().runTaskTimer(ValuxLobby.getInstance(), this, 20, 20);
		Bukkit.getPluginManager().registerEvents(this, ValuxLobby.getInstance());
	}

	@Override
	public void run() {
		practice.clearLores();
		kitpvp.clearLores();
		kitmap.clearLores();
		hcf.clearLores();
		
		practice.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + (ValuxLobby.getInstance().getServerCounts().get("practice-eu") + ValuxLobby.getInstance().getServerCounts().get("practice-us"))
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 3000);
		
		kitpvp.lore(ChatColor.GRAY + "Status: " + getStatus("kitpvp"))
		.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitpvp-eu") 
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500)
		.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
		
		kitmap.lore(ChatColor.GRAY + "Status: " + getStatus("kitmap"))
		.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitmap-eu") 
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500)
		.lore(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0);
		
		hcf.lore(ChatColor.GRAY + "Players" + ChatColor.GRAY + ": " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("hcf") 
				+ ChatColor.GRAY + "/" + ChatColor.AQUA + 500);
		
		ItemStack stack = practice.stack();
		
		Potion potion = new Potion(PotionType.INSTANT_HEAL);
		potion.setSplash(true);
		
		potion.apply(stack);
		
		inventory.setItem(10, stack);
		inventory.setItem(12, kitpvp.stack());
		inventory.setItem(14, hcf.stack());
		inventory.setItem(16, kitmap.stack());
		
		for (HumanEntity entity : inventory.getViewers()) {
			Player player = (Player) entity;
			
			player.updateInventory();
		}
	}
	
	private String getStatus(String server) {
		if (ServerList.contains(server)) return ChatColor.GREEN + "Online";
		
		return ChatColor.RED + "Offline";
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getInventory().getTitle() != null && !event.getInventory().getTitle().equalsIgnoreCase(inventory.getTitle())) return;		
		if (event.getCurrentItem() == null) return;
		
		ItemStack stack = event.getCurrentItem();
		ItemMeta meta = stack.getItemMeta();
		
		if (meta == null) return;		
		
		if (meta.getDisplayName().equalsIgnoreCase(practice.getName())) {
			event.getWhoClicked().closeInventory();
			event.getWhoClicked().openInventory(ValuxLobby.getInstance().getPracticInventory().getInventory());
			return;
		} else if (meta.getDisplayName().equalsIgnoreCase(kitpvp.getName())) {
			RedisMessenger.submit(new RedisPlayerSummonMessage(event.getWhoClicked().getName(), "kitpvp"));
			return;
		} else if (meta.getDisplayName().equalsIgnoreCase(kitmap.getName())) {
			RedisMessenger.submit(new RedisPlayerSummonMessage(event.getWhoClicked().getName(), "kitmap"));
			return;
		} else if (meta.getDisplayName().equalsIgnoreCase(hcf.getName())) {
			RedisMessenger.submit(new RedisPlayerSummonMessage(event.getWhoClicked().getName(), "hcf"));
			return;
		}
	}

}
