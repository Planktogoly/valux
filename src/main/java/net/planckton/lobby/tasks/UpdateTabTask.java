package net.planckton.lobby.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import net.planckton.lobby.ValuxLobby;
import net.valux.commons.tablist.Tablist;
import net.valux.commons.tablist.TablistItem;

public class UpdateTabTask implements Runnable {

	@Override
	public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
		    Tablist tablist = Tablist.getTablist(player);
		    
	        tablist.set(8, 1, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-eu") + " §2"));
	        tablist.set(8, 2, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("practice-us") + " §1"));
	        tablist.set(8, 3, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitpvp-eu") + " §3"));
	        tablist.set(9, 1, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§b"));
	        tablist.set(9, 2, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§a"));
	        tablist.set(9, 3, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§c"));
	        tablist.set(12, 1, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("kitmap-eu") + " §4"));
	        tablist.set(12, 2, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("hcf") + " §5"));
	        tablist.set(13, 1, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§7"));
	        tablist.set(13, 2, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§8"));
	        tablist.set(13, 3, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA + ValuxLobby.getInstance().getServerCounts().get("global") + " §8"));

	        tablist.write(player);
		}
	}

}
