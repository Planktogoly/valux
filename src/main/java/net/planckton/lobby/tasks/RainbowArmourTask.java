package net.planckton.lobby.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import net.valux.db.api.values.Rank;

public class RainbowArmourTask implements Runnable {
	
	private int r = 255;
	private int g = 0;
	private int b = 0;
	
	@Override
	public void run() {
		if (r == 255 && b == 0) g += 5;
		if (g == 255 && b == 0) r -= 5;
		if (r == 0 && g == 255) b += 5;
		if (b == 255 && r == 0) g -= 5;
		if (g == 0 && b == 255) r += 5;		
		if (r == 255 && g == 0) b -= 5;		
		
		Color color = Color.fromRGB(r, g, b);
		
		Bukkit.getOnlinePlayers().stream().filter(player -> Rank.getRank(player).inheritsFrom(Rank.ADMIN)).forEach(player -> {
			PlayerInventory inventory = player.getInventory();
			
			ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
		        helmetMeta.setColor(color);			
			helmet.setItemMeta(helmetMeta);
			
			ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
			
                        chestplateMeta.setColor(color);			
			chestplate.setItemMeta(chestplateMeta);
			
			ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta leggingsMeta = (LeatherArmorMeta) leggings.getItemMeta();
			
                        leggingsMeta.setColor(color);			
			leggings.setItemMeta(leggingsMeta);
			
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();
			bootsMeta.setColor(color);			
			boots.setItemMeta(bootsMeta);
			
			inventory.setHelmet(helmet);
			inventory.setChestplate(chestplate);
			inventory.setLeggings(leggings);
			inventory.setBoots(boots); 
		});
	}

}
