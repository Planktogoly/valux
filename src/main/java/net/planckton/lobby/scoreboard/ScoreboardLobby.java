package net.planckton.lobby.scoreboard;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import net.planckton.lobby.ValuxLobby;
import net.valux.commons.scoreboard.PlayerScoreboard;

public class ScoreboardLobby extends PlayerScoreboard {

	public ScoreboardLobby(Player p) {
		super(p);		
		reloadTeams();
	}

	@Override
	public void run() {
		setTitle(ChatColor.AQUA.toString() + ChatColor.BOLD + "Valux Network");	
		
		addLine(ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH + "---------------------");
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " Practice-EU" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("practice-eu"));
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " Practice-US" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("practice-us"));
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " KitPvP" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("kitpvp-eu"));
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " KitMap" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("kitmap-eu"));
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " Lobbies" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("hub"));
		addLine(ChatColor.GRAY.toString() + " »" + ChatColor.WHITE + " Global" + ": " + ChatColor.AQUA
		+ ValuxLobby.getInstance().getServerCounts().get("global"));
		addLine(ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH + "---------------------");				
	}

}
