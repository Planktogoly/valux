package net.planckton.lobby.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import lombok.Getter;

public class FancyItem {
	public static FancyItem ITEM(Material material) {
		return ITEM(material, 1);
	}
	
	public static FancyItem ITEM(Material material, int amount) {
		return new FancyItem(material, amount);
	}
	
	public static FancyItem ITEM(Material material, int amount, int data) {
		return new FancyItem(material, amount, (byte) data);
	}
	
	public static FancyItem ITEM(ItemStack stack) {
		return new FancyItem(stack);
	}
	
	private ItemStack stack;
	@Getter private String name;
	private ArrayList<String> lores;
	@Getter private boolean unbreakable;
	private ArrayList<ItemFlag> flags;
	@Getter private int randomAddAmount = 0;
	
	private ArrayList<String> canPlaceOn;
	private ArrayList<String> canDestroy;
	private HashMap<String,Object> nbt;
	
	private Color color;
	private String skullOwner;
	
	private boolean useLeatherMeta = false;
	private boolean useSkullMeta = false;
	
	public FancyItem(FancyItem item) {
		stack = item.stack.clone();
		name = item.name;
		if(item.lores != null) {
			lores = new ArrayList<String>();
			lores.addAll(item.lores);
		}
		unbreakable = item.unbreakable;
		if(item.flags != null) {
			flags = new ArrayList<ItemFlag>();
			flags.addAll(item.flags);
		}
		randomAddAmount = item.randomAddAmount;
		
		if(item.canPlaceOn != null) {
			canPlaceOn = new ArrayList<String>();
			canPlaceOn.addAll(item.canPlaceOn);
		}
		if(item.canDestroy != null) {
			canDestroy = new ArrayList<String>();
			canDestroy.addAll(item.canDestroy);
		}
		
		if(item.nbt != null) {
			nbt = new HashMap<String,Object>();
			nbt.putAll(item.nbt);
		}
	}
	
	public FancyItem(ItemStack stack) {
		this.stack = stack;
	}
	
	public FancyItem(Material material) {
		this(material, 1);
	}
	
	public FancyItem(Material material, int amount) {
		stack = new ItemStack(material, amount, (byte)0);
	}
	
	public FancyItem(Material material, int amount, byte data) {
		stack = new ItemStack(material, amount, data);
	}
		
	public FancyItem lore(String ...lores) {
		if(this.lores == null) this.lores = new ArrayList<String>();
		
		for(String lore : lores) {
			this.lores.add(lore);
		}
		
		return this;
	}
	
	public FancyItem lore(String lore) {
		if(lores == null) lores = new ArrayList<String>();
		
		lores.add(lore);
		
		return this;
	}
	
	public FancyItem name(String name) {
		this.name = name;
		
		return this;
	}
	
	public FancyItem enchant() {
		hideFlag(ItemFlag.HIDE_ENCHANTS);
		return enchantUnsafe(Enchantment.SILK_TOUCH, 32);
	}
	
	public FancyItem enchant(Enchantment enchantment) {
		return enchant(enchantment, 1);
	}
	
	public FancyItem enchant(Enchantment enchantment, int level) {
		stack.addEnchantment(enchantment, level);
		return this;
	}
	
	public FancyItem enchantUnsafe(Enchantment enchantment, int level) {
		stack.addUnsafeEnchantment(enchantment, level);
		return this;
	}
	
	public FancyItem hideFlag(ItemFlag flag) {
		if(flags == null) flags = new ArrayList<ItemFlag>();
		
		flags.add(flag);
		return this;
	}
	
	public FancyItem setColor(Color color) {
		if (!useLeatherMeta) {
			try {
				throw new Exception("Set UseLeatherMeta to true before using setColor()");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		this.color = color;
		
		return this;
	}
	
	public FancyItem setSkullOwner(String skullOwner) {
		if (!useSkullMeta) {
			try {
				throw new Exception("Set UseSkullMeta to true before using setSkullMeta()");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		this.skullOwner = skullOwner;
		
		return this;
	}
	
	public FancyItem setSkullMeta(boolean value) {
		this.useSkullMeta = value;	
		
		return this;
	}
	
	public FancyItem setUseLeatherMeta(boolean value) {
		this.useLeatherMeta = value;	
		
		return this;
	}
	
	public FancyItem unbreakable() {
		return unbreakable(true);
	}
	
	public FancyItem unbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		
		return this;
	}
	
	public FancyItem setRandomAddAmount(int randomAddAmount) {
		this.randomAddAmount = randomAddAmount;
		
		return this;
	}
	
	public FancyItem canPlaceOn(Material... materials) {
		if(canPlaceOn == null) canPlaceOn = new ArrayList<String>();
		
		for(Material material : materials) {
			canPlaceOn.add(material.name().replace(' ', '_').toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canDestroy(Material... materials) {
		if(canDestroy == null) canDestroy = new ArrayList<String>();
		
		for(Material material : materials) {
			canDestroy.add(material.name().replace(' ', '_').toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canPlaceOn(String... materials) {
		if(canPlaceOn == null) canPlaceOn = new ArrayList<String>();
		
		for(String material : materials) {
			canPlaceOn.add(material.toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem canDestroy(String... materials) {
		if(canDestroy == null) canDestroy = new ArrayList<String>();
		
		for(String material : materials) {
			canDestroy.add(material.toLowerCase());
		}
		
		return this;
	}
	
	public FancyItem addNBT(String key, Object data) {
		if(nbt == null) nbt = new HashMap<String,Object>();
		
		nbt.put(key, data);
		
		return this;
	}
	
	public FancyItem setAmount(int amount) {
		stack.setAmount(amount);
		
		return this;
	}
	
	public FancyItem setMaterial(Material material) {
		stack.setType(material);
		return this;
	}
	
	public ItemStack stack() {
		ItemStack stack = new ItemStack(this.stack);
		
		if (useLeatherMeta && !useSkullMeta) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) stack.getItemMeta();
			
			leatherArmorMeta.setColor(color);			
			stack.setItemMeta(leatherArmorMeta);
		}
		
		if (useSkullMeta && !useLeatherMeta) {
			SkullMeta skullMeta = (SkullMeta) stack.getItemMeta();
			
			skullMeta.setOwner(skullOwner);			
			stack.setItemMeta(skullMeta);
		}
		
		ItemMeta meta = stack.getItemMeta();
	
		if(name != null) meta.setDisplayName(name);
		if(unbreakable) meta.spigot().setUnbreakable(true);
		
		if(lores != null) {
			List<String> metaLores = meta.getLore();
			if(metaLores == null) metaLores = lores;
			else metaLores.addAll(lores);
			meta.setLore(metaLores);
		}
		
		if(flags != null) for(ItemFlag flag : flags) {
			meta.addItemFlags(flag);
		}
		
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	//getters
	public Collection<String> getLores() {
		if(lores == null) return null;
		
		return Collections.unmodifiableCollection(lores);
	}

	public FancyItem hideAllFlags() {
		if(flags == null) flags = new ArrayList<ItemFlag>();
		
		for(ItemFlag flag : ItemFlag.values()) flags.add(flag);
		
		return this;
	}
	
	public Material getType() {
		return stack.getType();
	}

	public FancyItem setData(byte data) {
		stack.setDurability(data);
		return this;
	}

	public FancyItem clearLores() {
		if(lores == null) return this;
		
		lores.clear();
		
		return this;
	}

}
