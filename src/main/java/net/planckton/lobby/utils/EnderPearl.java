package net.planckton.lobby.utils;

import org.bukkit.entity.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class EnderPearl {
	
	@Getter private Entity pearl;
	@Getter private int taskId;
	
	

}
