package net.planckton.lobby.utils;

import java.math.BigDecimal;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import net.planckton.lobby.ValuxLobby;
import net.valux.db.api.values.Rank;

public class Utils {	
	
	public static void spawnFirework(Player player, Rank rank) {
		try {
			long last = player.valux().getValue("last.firework", 0L);
			if (last - System.currentTimeMillis() <= 0)
				player.valux().setValue("last.firework", System.currentTimeMillis() + 1000);
			double timeleft = (last - System.currentTimeMillis()) / 1000.0;
			if (last - System.currentTimeMillis() > 0)
				player.sendMessage(ChatColor.RED + "You can use this again in " + ChatColor.RED.toString()
						+ ChatColor.BOLD + new BigDecimal(timeleft).setScale(1, BigDecimal.ROUND_UP) + "s"
						+ ChatColor.RED + ".");
			else {
				Firework firework = (Firework) player.getWorld().spawnEntity(player.getLocation(),
						EntityType.FIREWORK);
				FireworkMeta fireworkMeta = firework.getFireworkMeta();

				Type type = Type.BALL;

				if (rank == Rank.VIP) type = Type.BALL_LARGE;
				if (rank == Rank.PRO) type = Type.CREEPER;
				if (rank.inheritsFrom(Rank.TRAINEE)) type = Type.STAR;

				Color color = Color.YELLOW;

				if (rank == Rank.VIP) color = Color.fromBGR(85, 85, 255);
				if (rank == Rank.PRO) color = Color.fromBGR(255, 85, 255);
				if (rank.inheritsFrom(Rank.TRAINEE)) color = Color.RED;

				FireworkEffect effect = FireworkEffect.builder().withFlicker().withTrail().with(type)
						.withColor(Color.WHITE, color).build();

				fireworkMeta.setPower(2);
				fireworkMeta.addEffect(effect);

				firework.setFireworkMeta(fireworkMeta);

				if (ValuxLobby.getInstance().getToggleFirework().contains(player)) {
					firework.setPassenger(player);
					/*Bukkit.getScheduler().runTaskLater(ValuxLobby.getInstance(), () -> {
						firework.setPassenger(player);
					}, 7);*/
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void equipArmor(PlayerInventory inventory, Player player) {
		ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
		
		Rank rank = Rank.getRank(player);
		
		if (rank == Rank.BASIC) helmetMeta.setColor(Color.YELLOW);	
		if (rank == Rank.VIP) helmetMeta.setColor(Color.fromRGB(85, 85, 255));	
		if (rank == Rank.PRO) helmetMeta.setColor(Color.fromRGB(255, 85, 255));
		if (rank == Rank.YOUTUBE) helmetMeta.setColor(Color.RED);	
		if (rank.inheritsFrom(Rank.TRAINEE)) helmetMeta.setColor(Color.AQUA);			
		helmet.setItemMeta(helmetMeta);
		
		ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
		
		if (rank == Rank.BASIC) chestplateMeta.setColor(Color.YELLOW);	
		if (rank == Rank.VIP) chestplateMeta.setColor(Color.fromRGB(85, 85, 255));	
		if (rank == Rank.PRO) chestplateMeta.setColor(Color.fromRGB(255, 85, 255));
		if (rank == Rank.YOUTUBE) chestplateMeta.setColor(Color.RED);		
		if (rank.inheritsFrom(Rank.TRAINEE)) chestplateMeta.setColor(Color.AQUA);			
		chestplate.setItemMeta(chestplateMeta);
		
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta leggingsMeta = (LeatherArmorMeta) leggings.getItemMeta();
		
		if (rank == Rank.BASIC) leggingsMeta.setColor(Color.YELLOW);	
		if (rank == Rank.VIP) leggingsMeta.setColor(Color.fromRGB(85, 85, 255));	
		if (rank == Rank.PRO) leggingsMeta.setColor(Color.fromRGB(255, 85, 255));
		if (rank == Rank.YOUTUBE) leggingsMeta.setColor(Color.RED);		
		if (rank.inheritsFrom(Rank.TRAINEE)) leggingsMeta.setColor(Color.AQUA);			
		leggings.setItemMeta(leggingsMeta);
		
		ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();
		
		if (rank == Rank.BASIC) bootsMeta.setColor(Color.YELLOW);	
		if (rank == Rank.VIP) bootsMeta.setColor(Color.fromRGB(85, 85, 255));	
		if (rank == Rank.PRO) bootsMeta.setColor(Color.fromRGB(255, 85, 255));
		if (rank == Rank.YOUTUBE) bootsMeta.setColor(Color.RED);		
		if (rank.inheritsFrom(Rank.TRAINEE)) bootsMeta.setColor(Color.AQUA);			
		boots.setItemMeta(bootsMeta);
		
		inventory.setHelmet(helmet);
		inventory.setChestplate(chestplate);
		inventory.setLeggings(leggings);
		inventory.setBoots(boots); 
	}

}
