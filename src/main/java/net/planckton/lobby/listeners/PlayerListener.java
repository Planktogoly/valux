package net.planckton.lobby.listeners;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.spigotmc.event.entity.EntityDismountEvent;

import net.planckton.lobby.ValuxLobby;
import net.planckton.lobby.scoreboard.ScoreboardLobby;
import net.planckton.lobby.utils.FancyItem;
import net.planckton.lobby.utils.Utils;
import net.valux.commons.loader.Manager;
import net.valux.commons.scoreboard.CustomScoreboard;
import net.valux.commons.scoreboard.PlayerScoreboard;
import net.valux.commons.tablist.Tablist;
import net.valux.commons.tablist.TablistItem;
import net.valux.db.api.values.Rank;

public class PlayerListener implements Listener {

	private final FancyItem SELECTOR = new FancyItem(Material.COMPASS).name(ChatColor.AQUA + "Server Selector")
			.lore(ChatColor.GRAY + "Right click to join a server.");

	private final FancyItem PEARLRIDER = new FancyItem(Material.ENDER_PEARL)
			.name(ChatColor.LIGHT_PURPLE + "Pearl Rider");
	private final FancyItem PLAYEROFF = new FancyItem(Material.INK_SACK, 1, (byte) 8).name(ChatColor.YELLOW
			+ "Toggle players " + ChatColor.GRAY + "(" + ChatColor.GREEN + "Visible" + ChatColor.GRAY + ")")
			.lore(ChatColor.GRAY + "Right click to make players invisible.");
	private final FancyItem PLAYERON = new FancyItem(Material.INK_SACK, 1, (byte) 10).name(ChatColor.YELLOW
			+ "Toggle players " + ChatColor.GRAY + "(" + ChatColor.RED + "Invisible" + ChatColor.GRAY + ")")
			.lore(ChatColor.GRAY + "Right click to make players visible.");

	private final FancyItem FIREWORKON = new FancyItem(Material.FIREWORK).name(ChatColor.DARK_PURPLE + "Firework" + ChatColor.GRAY + " - " + ChatColor.GREEN + "ON")
			.lore(ChatColor.DARK_AQUA + "TIP: " + ChatColor.AQUA
					+ "Left click to disable firework riding.");
	private final FancyItem FIREWORKOFF = new FancyItem(Material.FIREWORK).name(ChatColor.DARK_PURPLE + "Firework" + ChatColor.GRAY + " - " + ChatColor.RED + "OFF")
			.lore(ChatColor.DARK_AQUA + "TIP: " + ChatColor.AQUA
					+ "Left click to enable firework riding.");
	
	private Set<Player> cooldown;
	private Set<Player> hiddenPlayers;
	private HashMap<Player, net.planckton.lobby.utils.EnderPearl> onEnderPearl;

	public PlayerListener() {
		this.cooldown = new HashSet<>();
		this.onEnderPearl = new HashMap<>();
		this.hiddenPlayers = new HashSet<>();
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		PlayerInventory inv = player.getInventory();

		player.teleport(new Location(player.getWorld(), 0, 69, 0));
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2, true, false));

		inv.clear();

		inv.setItem(0, PLAYEROFF.stack());
		inv.setItem(3, SELECTOR.stack());
		inv.setItem(5, PEARLRIDER.stack());

		player.sendMessage(
				ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH + "-----------------------------------------");
		player.sendMessage(ChatColor.AQUA + "Welcome " + Rank.getRank(player).getNameTagColor() + player.getName()
				+ ChatColor.AQUA + ", to the Valux Network!");
		player.sendMessage(" ");
		player.sendMessage(ChatColor.AQUA + "Website: " + ChatColor.WHITE + "www.valux.net");
		player.sendMessage(ChatColor.AQUA + "Teamspeak: " + ChatColor.WHITE + "ts.valux.net");
		player.sendMessage(ChatColor.AQUA + "Twitter: " + ChatColor.WHITE + "www.twitter.com/ValuxNetwork");
		player.sendMessage(ChatColor.AQUA + "Discord: " + ChatColor.WHITE + "discord.valux.net");
		player.sendMessage(ChatColor.AQUA + "Store: " + ChatColor.WHITE + "store.valux.net");
		player.sendMessage(
				ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH + "-----------------------------------------");
		player.sendMessage(ChatColor.AQUA + "Connecting to " + ChatColor.DARK_AQUA + Manager.getServerName()
				+ ChatColor.AQUA + "...");

		if (Rank.getRank(player).inheritsFrom(Rank.BASIC)) {
			inv.setItem(8, FIREWORKOFF.stack());
			if (!Rank.getRank(player).inheritsFrom(Rank.ADMIN)) Utils.equipArmor(inv, player);
		}

		for (Player onlinePlayer : Bukkit.getOnlinePlayers())
			if (hiddenPlayers.contains(onlinePlayer))
				onlinePlayer.hidePlayer(player);

		initBoards(player);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		cooldown.remove(player);
		hiddenPlayers.remove(player);
		onEnderPearl.remove(player);
		ValuxLobby.getInstance().getToggleFirework().remove(player);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {	
		event.getPlayer().eject();
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (event.getRightClicked().getType() != EntityType.PLAYER) return;
		if (event.getPlayer().getItemInHand().getType() != Material.AIR) return;
		
		event.getRightClicked().setPassenger(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {	
		if (event.getItem() == null) return;
		if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (event.getItem().getType() == Material.FIREWORK) {
				if (ValuxLobby.getInstance().getToggleFirework().contains(event.getPlayer())) {
					ValuxLobby.getInstance().getToggleFirework().remove(event.getPlayer());
					event.getPlayer().getInventory().setItem(8, FIREWORKOFF.stack());
				} else {
					ValuxLobby.getInstance().getToggleFirework().add(event.getPlayer());
					event.getPlayer().getInventory().setItem(8, FIREWORKON.stack());
				}
			}
			
			Entity entity = event.getPlayer().getPassenger();
			if (entity == null) return;
			
			
			event.getPlayer().eject();
			
			entity.setVelocity(event.getPlayer().getLocation().getDirection().normalize().multiply(2));
			return;
		}
		if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

		Player player = event.getPlayer();
		event.setCancelled(true);		

		if (event.getItem().getType() == Material.FIREWORK) {
			Utils.spawnFirework(player, Rank.getRank(player));
			return;
		} else if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(PLAYEROFF.getName())) {
			try {
				long last = player.valux().getValue("last.invisible", 0L);
				if (last - System.currentTimeMillis() <= 0)
					player.valux().setValue("last.invisible", System.currentTimeMillis() + 3000);
				double timeleft = (last - System.currentTimeMillis()) / 1000.0;
				if (last - System.currentTimeMillis() > 0)
					player.sendMessage(ChatColor.RED + "You can use this again in " + ChatColor.RED.toString()
							+ ChatColor.BOLD + new BigDecimal(timeleft).setScale(1, BigDecimal.ROUND_UP) + "s"
							+ ChatColor.RED + ".");
				else {
					player.getInventory().setItem(0, PLAYERON.stack());
					hiddenPlayers.add(player);

					for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						player.hidePlayer(onlinePlayer);
					}
					player.sendMessage(ChatColor.AQUA + "Poof!");
					player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2, 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		} else if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(PLAYERON.getName())) {
			try {
				long last = player.valux().getValue("last.invisible", 0L);
				if (last - System.currentTimeMillis() <= 0)
					player.valux().setValue("last.invisible", System.currentTimeMillis() + 3000);
				double timeleft = (last - System.currentTimeMillis()) / 1000.0;
				if (last - System.currentTimeMillis() > 0)
					player.sendMessage(ChatColor.RED + "You can use this again in " + ChatColor.RED.toString()
							+ ChatColor.BOLD + new BigDecimal(timeleft).setScale(1, BigDecimal.ROUND_UP) + "s"
							+ ChatColor.RED + ".");
				else {
					player.getInventory().setItem(0, PLAYEROFF.stack());
					hiddenPlayers.remove(player);

					for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						player.showPlayer(onlinePlayer);
					}
					player.sendMessage(ChatColor.LIGHT_PURPLE + "Woosh!");
					player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 2, 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		} else if (event.getItem().getType() == Material.COMPASS) {
			player.openInventory(ValuxLobby.getInstance().getInventory().getInventory());
			return;
		} else if (event.getItem().getType() == Material.ENDER_PEARL) {
			player.updateInventory();

			if (event.getPlayer().getLocation().getPitch() >= 20) return;

			EnderPearl pearl = player.launchProjectile(EnderPearl.class);

			pearl.setVelocity(player.getLocation().getDirection());
			pearl.setPassenger(player);

			if (onEnderPearl.containsKey(player)) {
				//Bukkit.broadcastMessage("You have a pearl but you spawned another one");
				net.planckton.lobby.utils.EnderPearl enderpearl = onEnderPearl.get(player);
				if (enderpearl.getPearl() != null) enderpearl.getPearl().remove();
				Bukkit.getScheduler().cancelTask(enderpearl.getTaskId());
			}

			BukkitTask task = new BukkitRunnable() {
				
				final Player finalPlayer = player;

				@Override
				public void run() {
					if (pearl.getLocation().getBlock().getType() != Material.AIR) {
						//Bukkit.broadcastMessage("Pearl landed in something that is not air");
						net.planckton.lobby.utils.EnderPearl enderpearl = onEnderPearl.remove(finalPlayer);
						if (enderpearl != null && enderpearl.getPearl() != null) {
							enderpearl.getPearl().eject();
							enderpearl.getPearl().remove();
						}
						finalPlayer.teleport(finalPlayer.getLocation().add(0, 2, 0));
						cancel();
						return;
					}

					if (!onEnderPearl.containsKey(player)) {
						//Bukkit.broadcastMessage("Where are you in the list");
						net.planckton.lobby.utils.EnderPearl enderpearl = onEnderPearl.remove(finalPlayer);
						if (enderpearl != null && enderpearl.getPearl() != null) enderpearl.getPearl().remove();
						cancel();
						return;
					}

					pearl.setPassenger(finalPlayer);
					//Bukkit.broadcastMessage("passenger on enderpearl");
				}
			}.runTaskTimerAsynchronously(ValuxLobby.getInstance(), 5, 5);

			net.planckton.lobby.utils.EnderPearl enderPearl = new net.planckton.lobby.utils.EnderPearl(pearl, task.getTaskId());
			onEnderPearl.put(player, enderPearl);
			return;
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();

		if (player.getGameMode() == GameMode.CREATIVE)	return;

		if (!cooldown.contains(player)) player.setAllowFlight(true);
		else  player.setAllowFlight(false);

		if (player.isOnGround()) cooldown.remove(player);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDismount(EntityDismountEvent event) {
		if (event.getDismounted().getType() != EntityType.ENDER_PEARL) return;
		if (event.getEntityType() != EntityType.PLAYER) return;

		//Bukkit.broadcastMessage("dont dismount you fuck");
		event.getDismounted().remove();
		net.planckton.lobby.utils.EnderPearl pearl = onEnderPearl.remove((Player) event.getEntity());
		if (pearl != null) Bukkit.getScheduler().cancelTask(pearl.getTaskId());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		Player player = event.getPlayer();

		if (!cooldown.contains(player)) {
			event.setCancelled(true);
			cooldown.add(player);
			player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0f, 1.0f);
			player.setVelocity(player.getLocation().getDirection().multiply(1.6D).setY(0.9D));
			player.setAllowFlight(false);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onProjectileHit(ProjectileHitEvent event) {
		if (event.getEntityType() != EntityType.ENDER_PEARL) return;
		if (!(event.getEntity().getShooter() instanceof Player)) return;

		Player player = ((Player) event.getEntity().getShooter());

		//Bukkit.broadcastMessage("Hit event triggered Thank you minecraft");
		net.planckton.lobby.utils.EnderPearl pearl = onEnderPearl.remove(player);
		if (pearl != null) Bukkit.getScheduler().cancelTask(pearl.getTaskId());

		player.teleport(player.getLocation().add(0, 2, 0));
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equalsIgnoreCase("Server Selector")
				|| event.getInventory().getTitle().equalsIgnoreCase("Practice Selector")) {
			event.setCancelled(true);
			return;
		}

		if (event.getWhoClicked().getGameMode() == GameMode.CREATIVE) return;

		event.setCancelled(true);
	}

	@EventHandler
	public void onInventoryDrag(InventoryDragEvent event) {
		if (event.getInventory().getTitle().equalsIgnoreCase("Server Selector")
				|| event.getInventory().getTitle().equalsIgnoreCase("Practice Selector")) {
			event.setCancelled(true);
			return;
		}

		if (event.getWhoClicked().getGameMode() == GameMode.CREATIVE)
			return;

		event.setCancelled(true);
	}

	private void initBoards(Player player) {
		CustomScoreboard.setScoreboard(player, new ScoreboardLobby(player), true);
		PlayerScoreboard.reloadTeams(player);

		Tablist tablist = Tablist.getTablist(player);

		tablist.set(1, 1, new TablistItem("§1§7§m---------------"));
		tablist.set(1, 2, new TablistItem(ChatColor.DARK_AQUA + "Valux Network"));
		tablist.set(1, 3, new TablistItem("§b§7§m---------------"));
		tablist.set(2, 1, new TablistItem("§4§l"));
		tablist.set(2, 2, new TablistItem("§a§7§m---------------"));
		tablist.set(2, 3, new TablistItem("§5§l"));
		tablist.set(3, 1, new TablistItem("§0"));
		tablist.set(3, 2, new TablistItem("§1"));
		tablist.set(3, 3, new TablistItem("§2"));
		tablist.set(4, 1, new TablistItem("§3"));
		tablist.set(4, 2, new TablistItem(ChatColor.AQUA + "Server Stats"));
		tablist.set(4, 3, new TablistItem("§4"));
		tablist.set(5, 1, new TablistItem("§2§7§m---------------"));
		tablist.set(5, 2, new TablistItem("§3§7§m---------------"));
		tablist.set(5, 3, new TablistItem("§4§7§m---------------"));
		tablist.set(6, 1, new TablistItem("§9"));
		tablist.set(6, 2, new TablistItem("§a"));
		tablist.set(6, 3, new TablistItem("§b"));
		tablist.set(7, 1, new TablistItem(ChatColor.GREEN + "Practice-EU"));
		tablist.set(7, 2, new TablistItem(ChatColor.GREEN + "Practice-US"));
		tablist.set(7, 3, new TablistItem(ChatColor.YELLOW + "KitPvP"));
		tablist.set(8, 1, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("practice-eu") + " §2"));
		tablist.set(8, 2, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("practice-us") + " §1"));
		tablist.set(8, 3, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("kitpvp-eu") + " §3"));
		tablist.set(9, 1, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§b"));
		tablist.set(9, 2, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§a"));
		tablist.set(9, 3, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§c"));
		tablist.set(10, 1, new TablistItem("§5"));
		tablist.set(10, 2, new TablistItem("§6"));
		tablist.set(10, 3, new TablistItem("§7"));
		tablist.set(11, 1, new TablistItem(ChatColor.LIGHT_PURPLE + "KitMap"));
		tablist.set(11, 2, new TablistItem(ChatColor.RED + "HCF"));
		tablist.set(11, 3, new TablistItem("§8"));
		tablist.set(12, 1, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("kitmap-eu") + " §4"));
		tablist.set(12, 2, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("hcf") + " §5"));
		tablist.set(12, 3, new TablistItem(ChatColor.DARK_PURPLE + "Total"));
		tablist.set(13, 1, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§7"));
		tablist.set(13, 2, new TablistItem(ChatColor.GRAY + "Queue: " + ChatColor.AQUA + 0 + "§8"));
		tablist.set(13, 3, new TablistItem(ChatColor.GRAY + "Online: " + ChatColor.AQUA
				+ ValuxLobby.getInstance().getServerCounts().get("global") + " §8"));
		tablist.set(14, 1, new TablistItem("§c"));
		tablist.set(14, 2, new TablistItem("§d"));
		tablist.set(14, 3, new TablistItem("§e"));
		tablist.set(15, 1, new TablistItem("§f"));
		tablist.set(15, 2, new TablistItem("§k"));
		tablist.set(15, 3, new TablistItem("§l"));
		tablist.set(16, 1, new TablistItem("§m"));
		tablist.set(16, 2, new TablistItem("§n"));
		tablist.set(16, 3, new TablistItem("§o"));
		tablist.set(17, 1, new TablistItem("§3§l"));
		tablist.set(17, 2, new TablistItem("§2§l"));
		tablist.set(17, 3, new TablistItem("§1§l"));
		tablist.set(18, 1, new TablistItem("§5§7§m---------------"));
		tablist.set(18, 2, new TablistItem("§6§7§m---------------"));
		tablist.set(18, 3, new TablistItem("§7§7§m---------------"));
		tablist.set(19, 1, new TablistItem(ChatColor.AQUA + "Website" + ChatColor.WHITE + ":"));
		tablist.set(19, 2, new TablistItem(ChatColor.AQUA + "Store" + ChatColor.WHITE + ":"));
		tablist.set(19, 3, new TablistItem(ChatColor.AQUA + "TeamSpeak" + ChatColor.WHITE + ":"));
		tablist.set(20, 1, new TablistItem(ChatColor.GRAY + "valux.net"));
		tablist.set(20, 2, new TablistItem(ChatColor.GRAY + "shop.valux.net"));
		tablist.set(20, 3, new TablistItem(ChatColor.GRAY + "ts.valux.net"));

		tablist.write(player);
	}

}
