package net.planckton.lobby;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.planckton.lobby.utils.ReverseLineInputStream;

public class printLogCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) return false;
		
		if (label.equalsIgnoreCase("printlog")) {
		    BufferedReader reader = null;
		    try {
		    	reader = new BufferedReader(new InputStreamReader(new ReverseLineInputStream(new File("logs" + File.separator + "latest.log"))));
		    	reader = new BufferedReader(new FileReader(new File("logs" + File.separator + "latest.log")));
		        String line;
		        while ((line = reader.readLine()) != null) {
		        	sender.sendMessage(line);
		        }
		    }catch(IOException exception) {
		    	exception.printStackTrace();
		    } finally {
		        if (reader != null) {
		            try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
		        }
		    }
		} else if (label.equalsIgnoreCase("printNPC")) {
		    BufferedReader reader = null;
		    try {
		        reader = new BufferedReader(new InputStreamReader(new ReverseLineInputStream(new File("plugins" + File.separator + "ValuxLobby" + File.separator + "NPC.yml"))));
		        String line;
		        while ((line = reader.readLine()) != null) {
		        	sender.sendMessage(line);
		        }
		    }catch(IOException exception) {
		    	exception.printStackTrace();
		    } finally {
		        if (reader != null) {
		            try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
		        }
		    }
		}
		return false;
	}
}
