package net.planckton.lobby;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import net.planckton.lobby.inventory.CompassInventory;
import net.planckton.lobby.inventory.PracticeInventory;
import net.planckton.lobby.listeners.PlayerListener;
import net.planckton.lobby.listeners.WorldListener;
import net.planckton.lobby.tasks.RainbowArmourTask;
import net.planckton.lobby.tasks.UpdateTabTask;
import net.valux.PlayerList;
import net.valux.commons.fixes.EnderpearlPatch;
import net.valux.commons.scoreboard.ScoreboardUpdateTask;
import net.valux.commons.task.Task;

public class ValuxLobby extends JavaPlugin {
	
	@Getter private static ValuxLobby instance;
	
	@Getter public HashMap<String, Integer> serverCounts = new HashMap<String, Integer>();
	@Getter public Set<Player> toggleFirework;
	
	@Getter private CompassInventory inventory;
	@Getter private PracticeInventory practicInventory;
	
	@Override
	public void onEnable() {
		getCommand("printlog").setExecutor(new printLogCommand());
		instance = this;
		
		new BukkitRunnable() {
			
			public void run() {
				serverCounts.put("practice-eu", PlayerList.getPlayers(Arrays.asList("pr_hub-eu", "pr_ds-eu")).size());
				serverCounts.put("practice-us", PlayerList.getPlayers(Arrays.asList("pr_hub-us", "pr_ds-us")).size());
				serverCounts.put("hcf", PlayerList.getPlayers("hcf").size());
				serverCounts.put("kitpvp-eu", PlayerList.getPlayers("kitpvp").size());
				serverCounts.put("kitmap-eu", PlayerList.getPlayers("kitmap").size());
				serverCounts.put("hub", PlayerList.getPlayers("hub").size());
				serverCounts.put("global", PlayerList.getAllPlayers().size());
				serverCounts.put("tournaments-eu", PlayerList.getPlayers(Arrays.asList("tour-eu", "pr_ds-eu-t")).size());
				serverCounts.put("tournaments-us", PlayerList.getPlayers(Arrays.asList("tour-us", "pr_ds-us-t")).size());		
			}
		}.runTaskTimer(instance, 20, 20);
		
		toggleFirework = new HashSet<>();
					
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), instance);
		Bukkit.getPluginManager().registerEvents(new WorldListener(), instance);		
		Bukkit.getPluginManager().registerEvents(new EnderpearlPatch(), instance);
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new RainbowArmourTask(), 5, 5);		
		Bukkit.getScheduler().runTaskLater(instance, () -> {
			inventory = new CompassInventory();
			practicInventory = new PracticeInventory();
			Bukkit.getScheduler().runTaskTimerAsynchronously(this, new UpdateTabTask(), 20, 20);
			new Task(new ScoreboardUpdateTask(), 20, 20);
		}, 21);
	}
	
	@Override
	public void onDisable() {
		instance = null;
	}

}
